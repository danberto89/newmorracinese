import React, { useState } from 'react'
import '../CSS Components/DescriptionBody.css'
import { GameArea } from './GameArea/MorraCinese/MainArea/GameArea';
import { DefaultHome } from './HomePage/DefaultHomePage'







export interface EventClick{
    setPageGame : () => void;
    stateView: boolean; 
}
export const Description: React.FC<EventClick> = ({stateView , setPageGame}) => {
  
    return (

        <>

            <div className='row' style={{
                backgroundColor: 'rgba(94, 88, 99, 0.678)',
                borderRadius: '150px 0 150px 0',
                boxShadow: '5px 9px 8px #fff',
                margin: '0 auto',
                marginTop: '100px',
                color: '#fff',
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column',
            }}>

                {stateView ? <>
                    <DefaultHome />
                    <div className='row container-button'>
                        <a href='#' id='play-game' onClick={setPageGame}>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        Play the GAME
                    </a>
                    </div>
                </> :
                    <>
                        
                        <GameArea />    <div className='row container-button' id='backHome'>
                                <a href='#' id='play-game' onClick={setPageGame}>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                       Back TO Home Page
                       </a>
    
                        </div> 
                    </>}


            </div>
        </>

    )


}
