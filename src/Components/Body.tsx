import React, { memo, useState } from 'react'
import  {Description}  from './DescriptionBody';
import  {Header}  from './Header';
import '../CSS Components/Body.css';



export const Body: React.FC = () => {
    console.log('render2');
    const [game, setGame] = useState(false);
    
    const setPageGame = () => {
        // switch(game){
        //     case 1:
        //         setGame(0);
        //         break;
        //     case 0:
        //         setGame(1);
        //         break;
        //     default:
        //         setGame(0);
        //         break;
        // }
        setGame(!game);
      
    }
  
    return (
        <>
        
            <div  style={{
                minHeight: '1200px',
                backgroundImage: 'url(' + 'sfondo.jpg' + ')',
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat'
            }} >
                <Header setPageGame={setPageGame} />
                <div className='wrapper'>
                <Description  stateView={game} setPageGame={setPageGame}/>
               
                </div>
            </div>
        </>
    )
};
