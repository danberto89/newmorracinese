import { renderStatus} from "../AreaComponents/RenderStatus";


export function MorraCineseLogic(param: number,CPUSelectedItem:number) {
    
    if (param === CPUSelectedItem) {
        return renderStatus(3);//pareggio
    }
    else if ((param === 3 && CPUSelectedItem === 2) || (param === 2 && CPUSelectedItem === 1) || (param === 1 && CPUSelectedItem === 3)) {
        return renderStatus(1);//vittoria
    }
    else if ((param === 2 && CPUSelectedItem === 3) || (param === 1 && CPUSelectedItem === 2) || (param === 3 && CPUSelectedItem === 1)) {
        return renderStatus(2);//sconfitta
    } else {
        return renderStatus(0);//errore
        
    }
}
