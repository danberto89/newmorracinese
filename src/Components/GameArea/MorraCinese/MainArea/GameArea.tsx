import React, { memo, useState } from "react";
import "../../../../CSS Components/GameArea/MorraCinese/MorraCinese.css";
import { PrintCorrispectiveItem } from "../AreaComponents/PrintCorrispectiveItem";
import { MorraCineseLogic } from "../Utility/MorraCineseLogic";

export const GameArea: React.FC = memo(() => {
 
    const [viewState, setviewState] = useState('view-hidden');
    const [UserSelectedItem, setUserChoice] = useState(0);
    const [CPUSelectedItem, setCPUItem] = useState(0);
    const [ConteggioPartite, setConteggioPartite] = useState(0);
    console.log('render');
    const SetSasso = () => {
        setUserChoice(1);
        SetCPUChoice();
        setConteggioPartite(ConteggioPartite + 1);
        setClassView();
    };
    const SetCarta = () => {
        setUserChoice(2);
        SetCPUChoice();
        setConteggioPartite(ConteggioPartite + 1);
        setClassView();
    };
    const SetForbice = () => {
        setUserChoice(3);
        SetCPUChoice();
        setConteggioPartite(ConteggioPartite + 1);
        setClassView();
    };
    const SetCPUChoice = () => {
        setCPUItem(() => Math.floor(Math.random() * 3) + 1);
    };

    const setClassView = () => {
        setviewState('view-visible');
    }

    

   
    return (
        <>

            <div className="container text-white ">
                <div className="col-md-8 center ">
                    <div className="row ">
                        <div className="text-center full-size">
                            <h2 className='display-4'>Gioca contro CPU</h2>
                            <h4>Mossa Numero: {ConteggioPartite}</h4>
                        </div>
                    </div>

                    <div className="row text-center m-Height-250 mt-5 label-size" >
                        <div className="col-md-6 ">
                            <div className="row text-center">
                                <div className="col-md-12 center ">
                                    <label className="d-inlineb" >Scelta giocatore1: </label>
                                </div>
                            </div>
                            <div className="row text-center">
                                <div className="col-md-12 center ">
                                    {PrintCorrispectiveItem(UserSelectedItem)}
                                </div>
                            </div>
                            <div className={"row text-center " + (viewState)} >
                                <div className="col-md-12 center ">

                                    {MorraCineseLogic(UserSelectedItem, CPUSelectedItem)}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="row text-center">
                                <div className="col-md-12 center ">
                                    <label className="d-inlineb" >Scelta CPU: </label>
                                </div>
                            </div>
                            <div className="row text-center">
                                <div className="col-md-12 center ">
                                    {PrintCorrispectiveItem(CPUSelectedItem)}
                                </div>
                            </div>
                            <div className={"row text-center " + (viewState)}>
                                <div className="col-md-12 center ">
                                    {MorraCineseLogic(CPUSelectedItem, UserSelectedItem)}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row text-center click-button">
                        <div className="col-md-12 center ">
                            <button className='button m-5' onClick={SetSasso}>Sasso</button>
                            <button className='button m-5' onClick={SetCarta}>Carta</button>
                            <button className='button m-5' onClick={SetForbice}>Forbice</button><br />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );

});
