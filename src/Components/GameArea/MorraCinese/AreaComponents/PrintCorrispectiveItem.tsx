import React from "react";


export function PrintCorrispectiveItem(param: number) {
    switch (param) {
        case 1:
            return <img src="/manoSasso.png" width="auto" height="190" alt="Sasso" />;
        case 2:
            return <img src="/manoCarta.png" width="auto" height="190" alt="Carta" />;
        case 3:
            return<img src="/manoForbice.png" width="auto" height="190" alt="Forbice" />;
        default:
            return <p>Effettua una scelta!!</p>;
    }
}
