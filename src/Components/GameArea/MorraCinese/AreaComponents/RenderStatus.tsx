import React, { useState } from "react";
import '../../../../CSS Components/GameArea/MorraCinese/RenderStatus.css';

const stringRender = (param: string) => {//param sostituibile co url link o path o con un oggetto immagine

    return <h2 className={"risultato-mossa " } >    {param}  </h2>
}

export function renderStatus(param: number) {
    switch (param) {
        case 1://vittoria
        return stringRender("Vittoria");

        case 2://sconfitta
        return stringRender("Sconfitta");

        case 3://pareggio
        return stringRender("Pareggio");

        default://errore
        return stringRender("Errore nei parametri");
    }
}